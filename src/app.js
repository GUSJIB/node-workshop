const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.post('/add', (req, res) => {
  res.send({
    result: "OK"
  })
})

app.get('/list', (req, res) => {
  res.send([{
    "id": 1
  },
  {
    "id": 3
  }])
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})